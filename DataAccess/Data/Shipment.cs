﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DataAccess.Data
{
    public class Shipment
    {
        
        [Key]
        public int Id { get; set; }
        public string ShipmentReferenceNumber { get; set; }

        public DateTime Created { get; set; } = DateTime.Now;

        public int StatusId { get; set; }
    }
}
