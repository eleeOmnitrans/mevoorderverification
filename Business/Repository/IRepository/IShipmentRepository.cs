﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace Business.Repository.IRepository
{
    public interface IShipmentRepository
    {
        public Task<ShipmentDTO> IsShipmentProcessed(string referenceNumber);
    }
}
