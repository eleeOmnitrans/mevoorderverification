﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class ShipmentDTO
    {
        public int Id { get; set; }
        public string ShipmentReferenceNumber { get; set; }

        public DateTime Created { get; set; } 

        public int StatusId { get; set; }
    }
}
