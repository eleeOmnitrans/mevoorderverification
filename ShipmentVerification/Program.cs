﻿using System;
using System.Configuration;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;
using DataAccess;
using System.Linq;
using System.Net.Mail;
using System.Net;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Data.SqlClient;
using System.Data;
using System.Collections.Generic;

namespace ShipmentVerification
{
    class Program
    {
        static void Main(string[] args)
        {
            string cn = ConfigurationManager.ConnectionStrings["AzureAPP"].ConnectionString;
            string InputExcelFolder = ConfigurationManager.AppSettings["InputExcelFolder"];
            string SBFolder = ConfigurationManager.AppSettings["SBFolder"];

            List<string> lstClientReferenceNumber = new List<string>();

            try
            {


                #region Read csv file input
                //scan InputExcelFolder
                DirectoryInfo d = new DirectoryInfo(InputExcelFolder);
                FileInfo[] Files = d.GetFiles("*.csv");

                foreach (FileInfo file in Files)
                {


                    string FileName = file.Name;
                    string FileNameFullPath = file.FullName;
                    Console.WriteLine("Processing {0}", FileNameFullPath);

                    StreamReader sr = new StreamReader(FileNameFullPath);
                    string line = "";
                    string ClientReferenceNumber = "";

                    while ((line = sr.ReadLine()) != null)
                    {
                        if (line.Length > 0)
                        {
                            ClientReferenceNumber = line;

                            #region check in database

                            if (IsTransactionInSBFolder(ClientReferenceNumber))
                            {

                            }
                            //else if (IsTransactionProcessedAndPushed(ClientReferenceNumber))
                            //{

                            //}
                            //else if (IsTransactionProcessed(ClientReferenceNumber))
                            //{

                            //}
                            else
                            {
                                //transaction not found/ not processed
                                //log to database
                                lstClientReferenceNumber.Add(ClientReferenceNumber);
                            }
                            #endregion
                        }
                    }



                    if (lstClientReferenceNumber.Count > 0)
                    {
                        //send out email notification
                        #region send out email notification
                        string bodyText = "The following transactions not found:";
                        foreach (string s in lstClientReferenceNumber)
                        {
                            bodyText += "\n" + s;
                        }
                        //string bodytext = file.Name + " is processed to " + OutputExcelFolder;
                        MailMessage message = new MailMessage();
                        SmtpClient smtp = new SmtpClient();
                        message.From = new MailAddress("ericlee.omnitrans@gmail.com");
                        message.To.Add(new MailAddress(ConfigurationManager.AppSettings["AlertEmailAddress"]));
                        message.Subject = file.Name + "is processed";
                        message.IsBodyHtml = false; //to make message body as html  
                        message.Body = bodyText;
                        smtp.Port = 587;
                        smtp.Host = "smtp.gmail.com"; //for gmail host  
                        smtp.EnableSsl = true;
                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = new NetworkCredential("ericlee.omnitrans@gmail.com", "mail2Omnitrans!!");
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                        smtp.Send(message);

                        #endregion
                    }
                }


                #endregion


                foreach (string s in lstClientReferenceNumber)
                {
                    Console.WriteLine(s);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //write to database log

                string errMsg = ex.Message;
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                message.From = new MailAddress("ericlee.omnitrans@gmail.com");
                message.To.Add(new MailAddress(ConfigurationManager.AppSettings["AlertEmailAddress"]));
                message.Subject = "Process Invoice execution error";
                message.IsBodyHtml = false; //to make message body as html  
                message.Body = errMsg;
                smtp.Port = 587;
                smtp.Host = "smtp.gmail.com"; //for gmail host  
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential("ericlee.omnitrans@gmail.com", "mail2Omnitrans!!");
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);
            }

        }


        public static bool IsTransactionInSBFolder(string TransactionNumber)
        {
            bool isFileFound = false;

            string SBFolder = ConfigurationManager.AppSettings["SBFolder"];
            string[] filePaths = Directory.GetFiles(SBFolder, "*" + TransactionNumber + "*.csv",SearchOption.AllDirectories);

            Console.Write("Search {0} in {1}", TransactionNumber, SBFolder);

            var containHeader = Array.FindAll(filePaths, s => s.Contains("_H"));
            var containDetail = Array.FindAll(filePaths, s => s.Contains("_D"));

            if (containHeader.Length > 0 && containDetail.Length > 0)
            {
                isFileFound = true;
                Console.Write(" Found");
            }
            else
            {
                Console.Write(" Not Found");
            }

            Console.WriteLine();

            return isFileFound;
        }

        public static bool IsTransactionProcessedAndPushed(string TransactionNumber)
        {
            bool isProcessed = false;

            SqlConnection sqlcnn = new SqlConnection();
            sqlcnn.ConnectionString = ConfigurationManager.ConnectionStrings["MTLVPROD3"].ConnectionString;
            sqlcnn.Open();
            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.Connection = sqlcnn;
            sqlcmd.CommandText = "MEVOTECH.IsTransactionProcessedAndPushed";
            sqlcmd.CommandType = CommandType.StoredProcedure;
            SqlParameter para = sqlcmd.Parameters.AddWithValue("@TransactionNumber", TransactionNumber);
            para = sqlcmd.Parameters.Add("@IsProcessed", SqlDbType.Bit);
            sqlcmd.Parameters["@IsProcessed"].Direction = ParameterDirection.Output;
            sqlcmd.ExecuteNonQuery();

            isProcessed = Convert.ToBoolean(sqlcmd.Parameters["@IsProcessed"].Value.ToString());

            sqlcnn.Close();
            sqlcnn.Dispose();



            return isProcessed;
        }

        public static bool IsTransactionProcessed(string TransactionNumber)
        {
            bool isProcessed = false;

            SqlConnection sqlcnn = new SqlConnection();
            sqlcnn.ConnectionString = ConfigurationManager.ConnectionStrings["MTLVPROD3"].ConnectionString;
            sqlcnn.Open();
            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.Connection = sqlcnn;
            sqlcmd.CommandText = "MEVOTECH.IsTransactionProcessed";
            sqlcmd.CommandType = CommandType.StoredProcedure;
            SqlParameter para = sqlcmd.Parameters.AddWithValue("@TransactionNumber", TransactionNumber);
            para = sqlcmd.Parameters.Add("@IsProcessed", SqlDbType.Bit);
            sqlcmd.Parameters["@IsProcessed"].Direction = ParameterDirection.Output;
            sqlcmd.ExecuteNonQuery();

            isProcessed = Convert.ToBoolean(sqlcmd.Parameters["@IsProcessed"].Value.ToString());

            sqlcnn.Close();
            sqlcnn.Dispose();



            return isProcessed;
        }
    }
}
